CREATE TABLE person(
    per_id         	    NUMERIC,
    per_name        	VARCHAR(70) NOT NULL,
    per_street      	VARCHAR(50),
    per_city        	VARCHAR(50),
    per_state       	VARCHAR(3),
    per_zip_code   	    NUMERIC,
    email           	NUMERIC(200),
    per_gender		    VARCHAR(10),		
    PRIMARY KEY(per_id)
);

CREATE TABLE company(
    comp_id         	NUMERIC,
    comp_name       	VARCHAR(50),
    comp_street     	VARCHAR(50),
    comp_city       	VARCHAR(50),
    comp_state      	VARCHAR(3),
    comp_zip_code   	NUMERIC,
    industry_group  	VARCHAR(100),
    website         	VARCHAR(200),
    PRIMARY KEY(comp_id)
);

CREATE TABLE job (
    job_code            NUMERIC,
    pos_code		    NUMERIC,
    emp_mode            VARCHAR(10),
    Pay_rate		    NUMERIC(6,2),
    pay_type		    VARCHAR(15),
    cate_code	        VARCHAR(15),
    PRIMARY KEY(job_code) 
    FOREIGN KEY(pos_code) REFERENCES position
);

CREATE TABLE position(
    pos_code        	NUMERIC,
    pos_title		    VARCHAR(50),
    pos_description	    VARCHAR(200),
    emp_mode        	VARCHAR(60),
    comp_id         	NUMERIC,
    pay_range_high	    NUMERIC,
    pay_range_low	    NUMERIC,
    job_code		    NUMERIC,
    PRIMARY KEY(pos_code)
    FOREIGN KEY(comp_id) REFERENCES company
);

CREATE TABLE phone(
    per_id      		NUMERIC,
    mobile_num     	 	VARCHAR(20),
    home_num			NUMERIC,
    work_num			NUMERIC,		
    PRIMARY KEY(per_id, mobile_num),
    FOREIGN KEY(per_id) REFERENCES person
);

CREATE TABLE works(
    per_id          	NUMERIC,
    job_code       		NUMERIC,
    start_date      	DATE NOT NULL,
    end_date       		DATE,
    PRIMARY KEY(job_code, per_id)
    FOREIGN KEY(per_id) REFERENCES person

);

CREATE TABLE section(
    c_code          	VARCHAR(10),
    sec_no          	NUMERIC,
    complete_date   	DATE,
    sec_year        	NUMERIC(40),
    offered_by      	VARCHAR(60),
    format      	    VARCHAR(60),
    price           	NUMERIC,
    PRIMARY KEY(c_code,sec_no,sec_year)
    FOREIGN KEY(c_code) REFERENCES Course

);

CREATE TABLE course(
    c_code              VARCHAR(20),
    title               VARCHAR(80),
    level               VARCHAR(20),
    description         VARCHAR(300),
    status              VARCHAR(80),
    retail_price        NUMERIC(4,2),	
    PRIMARY KEY(c_code)
);

CREATE TABLE has_skill(
    per_id     		NUMERIC,
    sk_code    		VARCHAR(10),
    PRIMARY KEY(per_id, sk_code),
    FOREIGN KEY(per_id) REFERENCES person
    FOREIGN KEY(sk_code) REFERENCES skill
);

CREATE TABLE skill(
   sk_code             	NUMERIC,
   sk_title             VARCHAR(60),
   sk_description   	VARCHAR(300),   		
   level		VARCHAR(20),
   PRIMARY KEY(sk_code)
);

CREATE TABLE teaches(
    c_code      		VARCHAR(20),
    sk_code     		NUMERIC,
    PRIMARY KEY(c_code, sk_code)
    FOREIGN KEY(sk_code) REFERENCES skill,
    FOREIGN KEY(c_code) REFERENCES course
);

CREATE TABLE prerequisite(
   c_code          		VARCHAR(20),
   required_code   		NUMERIC,
   PRIMARY KEY(c_code, required_code)
   FOREIGN KEY(c_code)REFERENCES course
);

CREATE TABLE requires(
    pos_code    			NUMERIC,
    sk_code     			NUMERIC,
    PRIMARY KEY(pos_code,sk_code),
    FOREIGN KEY(sk_code) REFERENCES skill,
    FOREIGN KEY(pos_code) REFERENCES position
);

CREATE TABLE required_by_job(
    job_code    			NUMERIC,
    sk_code     			NUMERIC,
    PRIMARY KEY(job_code,sk_code),
    FOREIGN KEY(sk_code) REFERENCES skill
    FOREIGN KEY(job_code) REFERENCES job
);

CREATE TABLE takes (
    per_id               NUMERIC,
    c_code               VARCHAR(20),
    sec_no               NUMERIC,
    complete_date        DATE,
    PRIMARY KEY (per_id, c_code, sec_no),
    FOREIGN KEY(per_id) REFERENCES person,
    FOREIGN KEY(c_code, sec_no) REFERENCES section
);

CREATE TABLE GICS (
    ind_id	         NUMERIC,
    ind_title	     VARCHAR(60),
    level	         VARCHAR(20),
    description      VARCHAR(300),
    parent_id	     NUMERIC,
    PRIMARY KEY(ind_id)
);

CREATE TABLE sub_industry (
    ind_id       NUMERIC,
    comp_id	     NUMERIC,
    PRIMARY KEY(ind_id,comp_id)
    FOREIGN KEY(comp_id) REFERENCES company
);