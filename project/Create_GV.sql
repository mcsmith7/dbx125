CREATE TABLE factory
(
	fac_id VARCHAR(3),
	fac_name VARCHAR(50) NOT NULL,
	address VARCHAR(100),
	zip_code VARCHAR(4),
	phone VARCHAR(10),
	manager VARCHAR(50),
	PRIMARY KEY (fac_id)
);

CREATE TABLE product
(
	p_code VARCHAR(4),
	p_name VARCHAR(50) NOT NULL,
	description VARCHAR(300),
	quantity INT,
	unit VARCHAR(10),
	avg_cost NUMERIC(18,2),
	PRIMARY KEY (p_code)
);

CREATE TABLE material
(
	m_code VARCHAR(5),
	m_name VARCHAR(30) NOT NULL,
	quantity NUMERIC(18,2),
	unit VARCHAR(10),
	min_level NUMERIC(18,2),
	PRIMARY KEY (m_code)
);

CREATE TABLE supplier
(
	sup_id VARCHAR(5),
	website VARCHAR(200),
	contact_email VARCHAR(100),
	PRIMARY KEY (sup_id)
);

CREATE TABLE customer
(
	cus_id VARCHAR(5),
	contact_person VARCHAR(100),
	contact_email VARCHAR(100),
	PRIMARY KEY (cus_id)
);
CREATE TABLE contract
(
	contract_id VARCHAR(5),
	cus_id VARCHAR(5),
	date DATE,
	sale_amount NUMERIC(18,2),
	pay_schedule VARCHAR(6),
	PRIMARY KEY (contract_id),
	FOREIGN KEY (cus_id) REFERENCES customer
	UNIQUE (pay_schedule)
);

CREATE TABLE lineItem
(
	contract_id VARCHAR(5),
	p_code VARCHAR(4),
	quantity INT,
	PRIMARY KEY (p_code, contract_id),
	FOREIGN KEY (p_code) REFERENCES product,
	FOREIGN KEY (contract_id) REFERENCES contract
);

CREATE TABLE purchase
(
	purchase_num VARCHAR(5),
	sup_id VARCHAR(5),
	sup_order_num VARCHAR(5) NOT NULL,
	book_date DATE,
	pay_date DATE,
	note VARCHAR(500),
	PRIMARY KEY (purchase_num),
	FOREIGN KEY (sup_id) REFERENCES supplier,
	UNIQUE (sup_order_num)
);

CREATE TABLE purchaseLine
(
	purchase_num VARCHAR(5),
	m_code VARCHAR(5),
	quantity INT,
	PRIMARY KEY (m_code, purchase_num),
	FOREIGN KEY (m_code) REFERENCES material,
	FOREIGN KEY (purchase_num) REFERENCES purchase
);

CREATE TABLE makes
(
	fac_id VARCHAR(3),
	p_code VARCHAR(4),
	quantity INT,
	PRIMARY KEY (fac_id, p_code),
	FOREIGN KEY (fac_id) REFERENCES factory,
	FOREIGN KEY (p_code) REFERENCES product
);

CREATE TABLE buys
(
	fac_id VARCHAR(3),
	m_code VARCHAR(5),
	quantity INT,
	PRIMARY KEY (fac_id, m_code),
	FOREIGN KEY (fac_id) REFERENCES factory,
	FOREIGN KEY (m_code) REFERENCES material
);

CREATE TABLE person
(
	per_ID NUMERIC,
	per_firstname VARCHAR(70) NOT NULL,
	per_lastname VARCHAR(70) NOT NULL,
	per_street VARCHAR(50),
	per_city VARCHAR(50),
	per_state VARCHAR(3),
	per_zip_code NUMERIC,
	email VARCHAR(200),
	per_gender VARCHAR(10),
	PRIMARY KEY(per_ID)
);

CREATE TABLE works
(
	per_ID NUMERIC,
	fac_id VARCHAR(3),
	start_date date NOT NULL,
	end_date date,
	PRIMARY KEY(fac_id, per_ID),
	FOREIGN KEY(fac_id) REFERENCES factory,
	FOREIGN KEY(per_ID) REFERENCES person
);

CREATE TABLE position
(
	pos_code NUMERIC(4,0),
	pos_title VARCHAR(50),
	pos_description VARCHAR(200),
	pay_range_high NUMERIC,
	pay_range_low NUMERIC,
	PRIMARY KEY(pos_code)
);

CREATE TABLE requires
(
    pos_code NUMERIC,
    sk_code NUMERIC,
    PRIMARY KEY(pos_code,sk_code),
    FOREIGN KEY(sk_code) REFERENCES skill,
    FOREIGN KEY(pos_code) REFERENCES position
);

CREATE TABLE required_by_fac
(
	fac_id VARCHAR(3),
    sk_code NUMERIC,
    PRIMARY KEY(fac_id,sk_code),
	FOREIGN KEY(fac_id) REFERENCES factory,
    FOREIGN KEY(sk_code) REFERENCES skill
);

CREATE TABLE job
(
	fac_id VARCHAR(3),
	pos_code NUMERIC(4,0),
	emp_mode VARCHAR(10),
	pay_rate NUMERIC(6,2),
	pay_type VARCHAR(15),
	cate_code VARCHAR,
	PRIMARY KEY(fac_id, pos_code),
	FOREIGN KEY (fac_id) REFERENCES factory,
	FOREIGN KEY (pos_code) REFERENCES position
);   

CREATE TABLE phone
(
	per_ID NUMERIC,
	mobile_num VARCHAR(20),
	home_num VARCHAR(20),
	work_num VARCHAR(20),
	primary key(per_ID, mobile_num),
	foreign key(per_id) references person
);

CREATE TABLE has_skill
(
    per_ID NUMERIC,
    sk_code VARCHAR(10),
    PRIMARY KEY(per_ID, sk_code),
    FOREIGN KEY(per_ID) REFERENCES person,
	FOREIGN KEY (sk_code) REFERENCES skill
);

CREATE TABLE skill
(
	sk_code NUMERIC,
	sk_title VARCHAR(60),
	sk_description VARCHAR(300),   
	level VARCHAR(20),
	PRIMARY KEY(sk_code)
);