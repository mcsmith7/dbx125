--factory(fac_id, fac_name, address, zip_code, phone, manager)
INSERT INTO factory VALUES('432', 'Demon Motor', '8673 Stillwater Drive Wadsworth, OH', '44281', '7112659193', 'John Smith');
INSERT INTO factory VALUES('543', 'Shadow Luxury', '7372 Miles Court Jamaica, NY', '11432', '6758447400', 'Baker Albrecht');
INSERT INTO factory VALUES('765', 'Dragon Trucks', '791 Center Street Butte, MT', '59701', '4784569193', 'Dougie Wilde');
INSERT INTO factory VALUES('382', 'Enigmotors', '9671 Depot Avenue Hartsville, SC', '29550', '4783927992', 'Thelma Meyer');
INSERT INTO factory VALUES('012', 'Motor Mountain', '9742 Riverview Avenue Saint Cloud, MN', '56301', '7583498792', 'Zoey Berry');
INSERT INTO factory VALUES('132', 'Autoify', '207 Hickory Lane Jamestown, NY', '14701', '8599340923', 'Jonas Benton');
INSERT INTO factory VALUES('948', 'Oyster Auto', '973 Sugar Street Bethel Park, PA', '15102', '5789347598', 'Isaac Douglas');
INSERT INTO factory VALUES('229', 'Motorzilla', '8295 Charles Street Lilburn, GA', '30047', '4832797492', 'Sky Garner');
INSERT INTO factory VALUES('467', 'Vortex Motor', '746 NE. Lyme Road Desoto, TX', '75115', '5231890128', 'Bobby Cabrera');
INSERT INTO factory VALUES('504', 'Tough Trucks', '1000 Orleans Ave New Orleans, NO', '70124', '5041234567', 'Drew Brees');

--material(m_code, m_name, quantity, unit, min_level)
INSERT INTO material VALUES('63271', 'Steel', 10000000, 'kg', 900);
INSERT INTO material VALUES('13478', 'Aluminum', 1200000, 'kg', 155);
INSERT INTO material VALUES('38789', 'Glass', 900000, 'm3', 40);
INSERT INTO material VALUES('57271', 'Rubber', 140000, 'kg', 70);
INSERT INTO material VALUES('95471', 'Copper', 2000000, 'kg', 200);
INSERT INTO material VALUES('27290', 'Titanium', 48900, 'kg', 50);
INSERT INTO material VALUES('44271', 'Iron', 70000, 'kg', 150);

--product(p_code, p_name, description, quantity, unit, avg_cost)
INSERT INTO product VALUES('7725', 'Buffalo', 'Gas', 4379, 'SUV', 26789.74);
INSERT INTO product VALUES('4623', 'Eagle', 'Gas', 232, 'Sedan', 38210.38);
INSERT INTO product VALUES('3217', 'Freedom', 'Diesel', 9349, 'Truck', 40002.28);
INSERT INTO product VALUES('5029', 'Mammoth', 'Diesel', 3213, 'SUV', 60089.21);
INSERT INTO product VALUES('1218', 'Vine', 'Electric', 4790, 'Van', 27893.37);
INSERT INTO product VALUES('0745', 'Titan', 'Hybrid', 8315, 'Sedan', 19203.29);
INSERT INTO product VALUES('4320', 'Enigma', 'Electric', 4639, 'Sedan', 41372.28);
INSERT INTO product VALUES('5809', 'Universe', 'Hybrid', 3215, 'SUV', 23289.70);
INSERT INTO product VALUES('2135', 'Warrior', 'Gas', 82, 'Sportscar', 90321.21);
INSERT INTO product VALUES('4001', 'Ranger', 'Gas', 1280, 'Truck' , 43728.19);

-- contract(contract_id, cus_id, date(yyyy-mm-dd), sale_amount, pay_schedule)
INSERT INTO contract VALUES('37829', '27839', ('2018-11-14'), 133948.70, 'ABC123');
INSERT INTO contract VALUES('38374', '23871', ('2018-10-25'), 343893.42, 'FGH346');
INSERT INTO contract VALUES('38375', '23871', ('2018-03-08'), 534945.32, 'KLM456');
INSERT INTO contract VALUES('19032', '09821', ('2015-08-14'), 320018.24, 'QRS678');
INSERT INTO contract VALUES('21983', '89012', ('2016-07-09'), 600892.10, 'WXYZ90');
INSERT INTO contract VALUES('12389', '12879', ('2019-01-17'), 195253.59, 'ABC987');
INSERT INTO contract VALUES('71382', '21980', ('2014-10-24'), 576098.70, 'CDE654');
INSERT INTO contract VALUES('02013', '23165', ('2016-11-29'), 186317.60, 'FGH999');
INSERT INTO contract VALUES('87321', '98021', ('2012-03-12'), 378330.32, 'ZXY001');
INSERT INTO contract VALUES('12098', '82371', ('2013-04-13'), 1445139.36, 'ASD219');
INSERT INTO contract VALUES('23689', '94325', ('2019-08-10'), 393553.71, 'POR219');

--lineItem(contract_id, p_code, quantity)
INSERT INTO lineItem VALUES('37829', '7725', 5);
INSERT INTO lineItem VALUES('38374', '4623', 9);
INSERT INTO lineItem VALUES('38375', '4623', 14);
INSERT INTO lineItem VALUES('19032', '3217', 8);
INSERT INTO lineItem VALUES('21983', '5029', 10);
INSERT INTO lineItem VALUES('12389', '1218', 7);
INSERT INTO lineItem VALUES('71382', '0745', 30);
INSERT INTO lineItem VALUES('02013', '4320', 11);
INSERT INTO lineItem VALUES('87321', '5809', 8);
INSERT INTO lineItem VALUES('12098', '2135', 16);
INSERT INTO lineItem VALUES('23689', '4001', 9);

-- purchase(purchase_num, sup_id, sup_order_num, book_date(yyyy-mm-dd), pay_date(yyyy-mm-dd), note)
INSERT INTO purchase VALUES('12893', '47983', '28231', ('2018-01-14'), ('2018-01-19'), null);
INSERT INTO purchase VALUES('32081', '21098', '79843', ('2016-10-26'), ('2016-11-20'), null);
INSERT INTO purchase VALUES('19128', '83724', '63482', ('2020-02-19'), ('2020-02-25'), null);
INSERT INTO purchase VALUES('84392', '47983', '32185', ('2018-06-21'), ('2018-10-24'), null);
INSERT INTO purchase VALUES('84393', '47983', '32186', ('2018-07-18'), ('2018-12-14'), null);
INSERT INTO purchase VALUES('97812', '21098', '85132', ('2017-05-18'), ('2017-08-18'), null);
INSERT INTO purchase VALUES('37928', '83724', '74932', ('2018-03-03'), ('2018-11-09'), null);
INSERT INTO purchase VALUES('58904', '79043', '21432', ('2019-09-18'), ('2019-10-18'), null);
INSERT INTO purchase VALUES('32718', '79043', '32150', ('2016-03-19'), ('2016-03-22'), null);
INSERT INTO purchase VALUES('12280', '47983', '32816', ('2017-08-20'), ('2017-08-19'), null);
INSERT INTO purchase VALUES('92301', '47329', '68930', ('2019-11-03'), ('2019-12-02'), null);

-- purchaseLine(purchase_num, m_code, quantity)
INSERT INTO purchaseLine VALUES('12893', '38789', 2000);
INSERT INTO purchaseLine VALUES('32081', '63271', 11000);
INSERT INTO purchaseLine VALUES('19128', '95471', 9401);
INSERT INTO purchaseLine VALUES('84392', '27290', 10001);
INSERT INTO purchaseLine VALUES('84393', '57271', 10000);
INSERT INTO purchaseLine VALUES('97812', '63271', 9804);
INSERT INTO purchaseLine VALUES('37928', '57271', 1735);
INSERT INTO purchaseLine VALUES('58904', '13478', 7320);
INSERT INTO purchaseLine VALUES('32718', '63271', 10407);
INSERT INTO purchaseLine VALUES('12280', '44271', 1695);
INSERT INTO purchaseLine VALUES('92301', '13478', 7138);

-- supplier(sup_id, website, contact_email)
INSERT INTO supplier VALUES('47983', 'rawmaterials4you.com', 'johnz@rawmaterials4you.com');
INSERT INTO supplier VALUES('79043', 'heavydutyproducts.com', 'tom2193@gmail.com');
INSERT INTO supplier VALUES('21098', 'steelnthings.com', 'matthew3@steelnthings.com');
INSERT INTO supplier VALUES('83724', 'factorymetals.com', 'zekeappleseed@yahoo.com');
INSERT INTO supplier VALUES('47329', 'glassnparts.com', 'charleyorgeron@glassnparts.com');

-- customer(cus_id, contact_person, contact_email)
INSERT INTO customer VALUES('27839', 'Cory Mason', 'cmason123@gmail.com');
INSERT INTO customer VALUES('23871', 'Fredrick Waters', 'fredwater321@hotmail.com');
INSERT INTO customer VALUES('09821', 'Saul Miller', 'saulmiller@yahoo.com');
INSERT INTO customer VALUES('89012', 'Angela Swanson', 'angieswan@gmail.com');
INSERT INTO customer VALUES('12879', 'Morris Terry', 'moterry2@yahoo.com');
INSERT INTO customer VALUES('21980', 'Brendan Lamb', 'brendanlamb4@gmail.com');
INSERT INTO customer VALUES('23165', 'Mike Wise', 'mikewise32@gmail.com');
INSERT INTO customer VALUES('98021', 'Leticia Bennett', 'lbennet99@yahoo.com');
INSERT INTO customer VALUES('82371', 'Kristen Doyle', 'kristendoyle567@gmail.com');
INSERT INTO customer VALUES('94325', 'Wesley Franklin', 'wesfrank007@hotmail.com');

-- makes(fac_id, p_code, quantity)
INSERT INTO makes VALUES('432', '7725', 90);
INSERT INTO makes VALUES('432', '4623', 32);
INSERT INTO makes VALUES('543', '4623', 85); 
INSERT INTO makes VALUES('543', '5029', 320); 
INSERT INTO makes VALUES('765', '3217', 100); 
INSERT INTO makes VALUES('382', '5029', 118); 
INSERT INTO makes VALUES('382', '0745', 102); 
INSERT INTO makes VALUES('382', '5809', 31); 
INSERT INTO makes VALUES('012', '1218', 43);
INSERT INTO makes VALUES('012', '7725', 121);
INSERT INTO makes VALUES('012', '4320', 128);
INSERT INTO makes VALUES('132', '0745', 63);
INSERT INTO makes VALUES('132', '2135', 123);
INSERT INTO makes VALUES('948', '4320', 129);
INSERT INTO makes VALUES('948', '7725', 159);
INSERT INTO makes VALUES('948', '1218', 111);
INSERT INTO makes VALUES('229', '5809', 200);
INSERT INTO makes VALUES('467', '2135', 72);
INSERT INTO makes VALUES('504', '4001', 79);
INSERT INTO makes VALUES('504', '3217', 60);

-- buys(fac_id, m_code, quantity)
INSERT INTO buys VALUES('432', '63271', 329);
INSERT INTO buys VALUES('432', '13478', 343);
INSERT INTO buys VALUES('432', '38789', 1234);
INSERT INTO buys VALUES('432', '57271', 8432);
INSERT INTO buys VALUES('432', '95471', 332);
INSERT INTO buys VALUES('543', '63271', 432);
INSERT INTO buys VALUES('543', '13478', 3420);
INSERT INTO buys VALUES('543', '38789', 3729);
INSERT INTO buys VALUES('543', '57271', 2190);
INSERT INTO buys VALUES('543', '95471', 2193);
INSERT INTO buys VALUES('543', '27290', 432);
INSERT INTO buys VALUES('543', '44271', 201);
INSERT INTO buys VALUES('765', '63271', 3218);
INSERT INTO buys VALUES('765', '13478', 981);
INSERT INTO buys VALUES('765', '38789', 291);
INSERT INTO buys VALUES('765', '57271', 421);
INSERT INTO buys VALUES('765', '95471', 432);
INSERT INTO buys VALUES('382', '63271', 312);
INSERT INTO buys VALUES('382', '13478', 3289);
INSERT INTO buys VALUES('382', '38789', 2100);
INSERT INTO buys VALUES('382', '57271', 4382);
INSERT INTO buys VALUES('382', '95471', 3218);
INSERT INTO buys VALUES('382', '27290', 9281);
INSERT INTO buys VALUES('012', '63271', 823);
INSERT INTO buys VALUES('012', '13478', 432);
INSERT INTO buys VALUES('012', '38789', 5432);
INSERT INTO buys VALUES('012', '57271', 432);
INSERT INTO buys VALUES('012', '95471', 312);
INSERT INTO buys VALUES('012', '44271', 531);
INSERT INTO buys VALUES('132', '63271', 543);
INSERT INTO buys VALUES('132', '13478', 532);
INSERT INTO buys VALUES('132', '38789', 3215);
INSERT INTO buys VALUES('132', '57271', 3920);
INSERT INTO buys VALUES('132', '95471', 4328);
INSERT INTO buys VALUES('948', '63271', 432);
INSERT INTO buys VALUES('948', '13478', 533);
INSERT INTO buys VALUES('948', '38789', 431);
INSERT INTO buys VALUES('948', '57271', 324);
INSERT INTO buys VALUES('948', '95471', 543);
INSERT INTO buys VALUES('948', '27290', 532);
INSERT INTO buys VALUES('948', '44271', 213);
INSERT INTO buys VALUES('229', '63271', 432);
INSERT INTO buys VALUES('229', '13478', 342);
INSERT INTO buys VALUES('229', '38789', 432);
INSERT INTO buys VALUES('229', '57271', 543);
INSERT INTO buys VALUES('229', '95471', 932);
INSERT INTO buys VALUES('467', '63271', 832);
INSERT INTO buys VALUES('467', '13478', 821);
INSERT INTO buys VALUES('467', '38789', 542);
INSERT INTO buys VALUES('467', '57271', 532);
INSERT INTO buys VALUES('467', '95471', 312);
INSERT INTO buys VALUES('467', '27290', 3532);


--person(per_ID, per_firstname, per_lastname per_street, per_city, per_state, per_zip_code, email, per_gender)
INSERT INTO person VALUES (1, 'Elena', 'Leonard', '9729 Pilgrim Lane', 'Taylors', 'SC', 29687, 'eleonard@gmail.com', 'female');
INSERT INTO person VALUES (2, 'Francis', 'King', '9881 Brandywine Rd', 'Altoona', 'PA', 16601, 'francisking@gmail.com','male');
INSERT INTO person VALUES (3, 'Anna', 'Holt', '42 Brookside Ave.', 'Garland', 'TX', 75043, 'annaholt@outlook.com', 'female');
INSERT INTO person VALUES (4, 'Daniel', 'Becker', '7092 Madison Road', 'Westwood', 'NJ', 07675, 'danielbecker@yahoo.com','male');
INSERT INTO person VALUES (5, 'Ron', 'Williams', '8679 La Sierra Ave.', 'Tucson', ' AZ', 85718, 'rwilliams@yahoo.com','male');

--position(pos_code, pos_title, pos_description, pay_range_high, pay_range_low)
INSERT INTO position VALUES (1100, 'Database Models','Create and manage database', 60000, 30000);
INSERT INTO position VALUES (2200, 'Application Development','Front and Back end application development', 45000, 35000);
INSERT INTO position VALUES (3300, 'Automotive Test Engineer','defines innovation and excellence in signal processing', 50000, 35000);
INSERT INTO position VALUES (4400, 'Controls Specialists','control checklist to assure all aspects of detailing', 40000, 60000);
INSERT INTO position VALUES (5500, 'IT project manager','In-depth technical ability and knowledge of Data Center', 80000, 20000);
INSERT INTO position VALUES (6600, 'Creative Coder','Use javaScript through multiple iterations', 80000, 60000);
INSERT INTO position VALUES (7700, 'Detail shop','Conduct maintenance for assigned equipment', 20000, 8000);
INSERT INTO position VALUES (8800, 'Mechanic', 'performs service and installation on customer vehicles', 45000, 30000);
INSERT INTO position VALUES (9900, 'Retail Sales Associates', 'sales through superior customer service', 20000, 9000);
INSERT INTO position VALUES (2000, 'Auto Parts Delivery Driver', 'assure safe delivery of parts', 45000, 20000);

--Skill(sk_code, sk_title, sk_description, level)
INSERT INTO skill VALUES (0001,'Java application skill', 'Ability to use java application', 'Beginner');
INSERT INTO skill VALUES (0002,'Linear Algebra', 'Ability to handle linear algebra problems', 'Advanced');
INSERT INTO skill VALUES (0003,'Database skill', 'Ability to handle databases', 'Medium');
INSERT INTO skill VALUES (0004,'Electrical Engineering skill', 'Ability to understand the electrical engineering behind cars', 'Advanced');
INSERT INTO skill VALUES (0005,'Automotive', 'knowledge about automotive', 'Beginner');
INSERT INTO skill VALUES (0006,'Mechanic skill', 'Excellent knowledge about car mechanics', 'Medium');
INSERT INTO skill VALUES (0007,'Leadership', 'Good leadership', 'Advanced');
INSERT INTO skill VALUES (0008,'Communication Skill','Ability to communicate well with the customers and others', 'Medium');
INSERT INTO skill VALUES (0009,'Python', 'Ability to use python well', 'Medium');
INSERT INTO skill VALUES (0010,'C', 'Ability to use C', 'Beginner');
INSERT INTO skill VALUES (0011,'Writing skill', 'Ability to communicate through writing, also use for writing documents', 'Medium');
INSERT INTO skill VALUES (0012,'Critical Thinking skill', 'Ability to solve hard problems', 'Advanced');
INSERT INTO skill VALUES (0013,'Problem Solving skill', 'Good at problem solving', 'Beginner');
INSERT INTO skill VALUES (0014,'Creative', 'Ability to be creative at work and making designs', 'Beginner');
INSERT INTO skill VALUES (0015,'Analytical skill', 'Good at Analytical thinking', 'Advanced');
INSERT INTO skill VALUES (0016,'Physics skill', 'Ability to use physics', 'Medium');
INSERT INTO skill VALUES (0017,'Presenting skill', 'Good at presenting', 'Beginner');
INSERT INTO skill VALUES (0018,'Organization skill', 'Ability to be organized at workplace', 'Beginner');
INSERT INTO skill VALUES (0019,'English skill', 'Ability to be able to use English Language well', 'Beginner');
INSERT INTO skill VALUES (0020,'Bilingual', 'Ability to use different language other than english', 'Medium');
INSERT INTO skill VALUES (0021,'Driving skill', 'Good at driving large vehicle', 'Advanced');
INSERT INTO skill VALUES (0022,'Flexible', 'Good at adapting in to any kind of environment', 'Beginner');
INSERT INTO skill VALUES (0023,'Money Handling skill', 'Ability to handle money well ', 'Beginner');
INSERT INTO skill VALUES (0024,'Customer service', 'Ability to handle customer well', 'Advanced');
INSERT INTO skill VALUES (0025,'Attentive', 'Ability to pay attention to details', 'Medium');
INSERT INTO skill VALUES (0026,'Disciplined', 'THIS WORKS FOR DRIVERS. ability to stay on track ', 'Medium');
INSERT INTO skill VALUES (0027,'Time Management skill', 'ability to stay on time', 'Beginner');
INSERT INTO skill VALUES (0028,'Reading Skill', 'good at understanding documents', 'Beginner');
INSERT INTO skill VALUES (0029,'Machine Learning', 'Good at understanding machines', 'Advanced');
INSERT INTO skill VALUES (0030,'Mapping Skill', 'Good at navigating directions', 'Medium');


--has_skill(per_ID, sk_code)
INSERT INTO has_skill VALUES (1,0001);
INSERT INTO has_skill VALUES (1,0008);
INSERT INTO has_skill VALUES (1,0013);
INSERT INTO has_skill VALUES (2,0009);
INSERT INTO has_skill VALUES (2,0013);
INSERT INTO has_skill VALUES (2,0010);
INSERT INTO has_skill VALUES (2,0017);
INSERT INTO has_skill VALUES (3,0023);
INSERT INTO has_skill VALUES (3,0020);
INSERT INTO has_skill VALUES (3,0022);
INSERT INTO has_skill VALUES (3,0007);
INSERT INTO has_skill VALUES (4,0004);
INSERT INTO has_skill VALUES (4,0005);
INSERT INTO has_skill VALUES (4,0007);
INSERT INTO has_skill VALUES (4,0017);
INSERT INTO has_skill VALUES (4,0027);
INSERT INTO has_skill VALUES (5,0029);
INSERT INTO has_skill VALUES (5,0021);


--requires(pos_code, sk_code)
INSERT INTO requires VALUES (1100,0001);
INSERT INTO requires VALUES (1100,0003);
INSERT INTO requires VALUES (1100,0008);
INSERT INTO requires VALUES (2200,0001);
INSERT INTO requires VALUES (2200,0013);
INSERT INTO requires VALUES (3300,0005);
INSERT INTO requires VALUES (3300,0006);
INSERT INTO requires VALUES (4400,0006);
INSERT INTO requires VALUES (4400,0016);
INSERT INTO requires VALUES (5500,0006);
INSERT INTO requires VALUES (5500,0007);
INSERT INTO requires VALUES (5500,0015);
INSERT INTO requires VALUES (5500,0010);
INSERT INTO requires VALUES (6600,0001);
INSERT INTO requires VALUES (6600,0014);
INSERT INTO requires VALUES (6600,0019);
INSERT INTO requires VALUES (6600,0025);
INSERT INTO requires VALUES (7700,0024);
INSERT INTO requires VALUES (7700,0025);
INSERT INTO requires VALUES (7700,0008);
INSERT INTO requires VALUES (8800,0005);
INSERT INTO requires VALUES (8800,0015);
INSERT INTO requires VALUES (8800,0024);
INSERT INTO requires VALUES (8800,0008);
INSERT INTO requires VALUES (9900,0024);
INSERT INTO requires VALUES (9900,0008);
INSERT INTO requires VALUES (9900,0019);
INSERT INTO requires VALUES (2000,0030);
INSERT INTO requires VALUES (2000,0027);
INSERT INTO requires VALUES (2000,0026);
INSERT INTO requires VALUES (2000,0021);

--Job(fac_id, pos_code, emp_mode, pay_rate, pay_type, cate_code)
INSERT INTO job VALUES ('432',1100,'Full-Time',40000.00,'Salary','Medium');
INSERT INTO job VALUES ('543',2200,'Part-Time',25.00,'Wage','Medium');
INSERT INTO job VALUES ('765', 3300,'Part-Time',30.00,'Wage','High');
INSERT INTO job VALUES ('382', 4400,'Full-Time',45000.00,'Salary', 'High');
INSERT INTO job VALUES ('012',5500,'Full-Time',50000.00,'Salary', 'High');
INSERT INTO job VALUES ('132',6600,'Full-Time',60000.00,'Salary', 'Medium');
INSERT INTO job VALUES ('948',7700,'Part-Time',19.00,'Wage', 'Medium');
INSERT INTO job VALUES ('229', 8800,'Full-Time',30.00,'Wage', 'Medium');
INSERT INTO job VALUES ('467', 9900,'Part-Time',16.00,'Wage', 'Low');
INSERT INTO job VALUES ('504',2000,'Full-Time',25.00,'Wage', 'Low');
INSERT INTO job VALUES ('504',9900,'Full-Time',25.00,'Wage', 'Low');
INSERT INTO job VALUES ('504',7700,'Full-Time',75.00,'Wage', 'Low');

--required_by_fac(fac_id, sk_code)
INSERT INTO required_by_fac VALUES ('432',0001);
INSERT INTO required_by_fac VALUES ('543',0003);
INSERT INTO required_by_fac VALUES ('765',0001);
INSERT INTO required_by_fac VALUES ('382',0006);
INSERT INTO required_by_fac VALUES ('012',0016);
INSERT INTO required_by_fac VALUES ('132',0010);
INSERT INTO required_by_fac VALUES ('948',0015);
INSERT INTO required_by_fac VALUES ('229',0001);
INSERT INTO required_by_fac VALUES ('467',0024);
INSERT INTO required_by_fac VALUES ('504',0024);
INSERT INTO required_by_fac VALUES ('432',0015);
INSERT INTO required_by_fac VALUES ('543',0008);
INSERT INTO required_by_fac VALUES ('765',0021);
INSERT INTO required_by_fac VALUES ('382',0030);

--phone(per_ID, mobile_num, home_num, work_num)
INSERT INTO phone VALUES (1,'803-201-4815', '803-200-3273', '');
INSERT INTO phone VALUES (2,'814-204-1927', '814-201-9499', '');
INSERT INTO phone VALUES (3,'972-240-8926', '972-205-8848', '');
INSERT INTO phone VALUES (4,'201-383-4112', '201-534-2652', '');
INSERT INTO phone VALUES (5,'520-229-6647', '520-230-3559', '');

--works(per_id, fac_id, start_date, end_date)
INSERT INTO works VALUES (1, '432', '30-MAR-16', '04-JUN-18');
INSERT INTO works VALUES (2, '543', '21-FEB-18', '12-SEP-19');
INSERT INTO works VALUES (3, '765', '20-MAY-19', NULL);
INSERT INTO works VALUES (4, '382', '05-JAN-19', NULL);
INSERT INTO works VALUES (5, '012', '09-AUG-17', NULL);