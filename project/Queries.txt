--1. List the workers by names in the alphabetical order of last names.
AZ:
SELECT DISTINCT per_firstname, per_lastname
FROM person NATURAL JOIN works NATURAL JOIN Store
WHERE end_date IS NULL
ORDER BY per_lastname ASC;

GV:
SELECT per_firstname, per_lastname
FROM person NATURAL JOIN works
WHERE end_date IS NULL
ORDER BY per_lastname ASC;


--2. List the staff (salary workers) by salary in descending order.
SELECT per_id, pay_rate
FROM job NATURAL JOIN works
WHERE pay_type = 'Salary'
AND end_date IS NULL
ORDER BY pay_rate DESC;


--3. List the average annual pay (the salary or wage rates multiplying by 1920 hours) of each store/factory in descending order.
AZ:
WITH total_salary AS
(
	SELECT store_id,
			CASE
				WHEN pay_type = 'Wage' then (pay_rate * 1920)
				else pay_rate 
			END AS annual_pay
	FROM job		
)
SELECT store_id, AVG(annual_pay)
FROM total_salary
GROUP BY store_id
ORDER BY AVG(annual_pay) DESC;

GV:
WITH total_salary AS
(
	SELECT fac_id,
			CASE
				WHEN pay_type = 'Wage' then (pay_rate * 1920)
				else pay_rate 
			END AS annual_pay
	FROM job		
)
SELECT fac_id, AVG(annual_pay)
FROM total_salary
GROUP BY fac_id
ORDER BY AVG(annual_pay) DESC;



--4. List the required skills of a given pos code in a readable format.
SELECT pos_code, sk_code, sk_title
FROM requires NATURAL JOIN skill;

--5. Given a person�s identifier, list this person�s skills in a readable format.
SELECT per_id, sk_code, sk_title
FROM has_skill NATURAL JOIN skill;

--6. Given a person�s identifier, list a person�s missing skills for a specific pos code in a readable format.
WITH missing_skills AS 
	((SELECT sk_code
	FROM requires)
	MINUS
	(SELECT sk_code
	FROM has_skill))
SELECT per_id, pos_code, sk_code, sk_title

--7
SELECT Item_num, SUM(Unit_cost) AS tot_sales, SUM(Quantity)
FROM purchase
WHERE Date between 20160101 and 20201231
GROUP BY Item_num
ORDER BY tot_sales Desc;

--8
SELECT item_num, MAX(tot_profit), title
FROM 
(SELECT inventory.item_num,SUM(unit_cost)*sales.Quantity AS tot_profit, title
FROM Inventory, sales
WHERE inventory.item_Num = sales.Item_num 
AND date BETWEEN 20180101 AND 20181231
GROUP BY title);

--9
SELECT item_num
FROM inventory
WHERE quantity < Min_leval;



-- 10. List the total sales in dollar to each customer of GV in 2018.
SELECT cus_id, SUM(sale_amount) AS total_sales
FROM contract NATURAL JOIN customer
WHERE  date >= '2018-01-01' 
AND date < '2019-01-01'
GROUP BY cus_id;
	
-- 11. Show m_code, m_name of the material(s) that GV purchased the most (measured by quantity) in the fourth quarter of 2018.
WITH total_2018 AS 
	(SELECT m_code, SUM(quantity) AS sum_quantity
	FROM purchase NATURAL JOIN purchaseLine
	WHERE pay_date >= '2018-10-01' 
	AND pay_date <= '2018-12-31'
	GROUP BY m_code)
SELECT m_code, m_name
FROM total_2018 NATURAL JOIN Material
WHERE sum_quantity = 
	(SELECT MAX(sum_quantity)
	FROM total_2018);


-- 12. Show the factory name that made the most total quantity of the product that was sold the most in 2018

WITH total_2018 AS 
	(SELECT p_code, SUM(quantity) AS sum_quantity
	FROM contract NATURAL JOIN lineItem
	WHERE date >= '2018-01-01' 
	AND date < '2019-01-01'
	GROUP BY p_code), 
max_2018 AS
	(SELECT p_code, MAX(sum_quantity) AS max_quant
	FROM total_2018)
SELECT fac_name	
FROM max_2018 NATURAL JOIN makes NATURAL JOIN factory
WHERE quantity = 
	(SELECT MAX(quantity)
	FROM max_2018 NATURAL JOIN makes);

--13. Given a person�s identifier, find all the jobs this person is currently holding and worked in the past.

SELECT per_id, pos_title, start_date, end_date
FROM works NATURAL JOIN position 
WHERE per_id = 1;

--14. In a local or national crisis, we need to find all the people who once held a position of the given pos code. List per id, name, job title and the years the person worked in (starting year and ending year).

SELECT per_id, per_name, pos_title, start_date, end_date 
FROM position NATURAL JOIN works NATURAL JOIN person
WHERE pos_code = 2200 AND end_date IS NOT NULL;

--15. Find all the unemployed people who once held a job position of the given pos code.

WITH unemployed AS (SELECT per_ID
				   FROM person
				   MINUS
				   SELECT per_ID
			           FROM works NATURAL JOIN position
				   WHERE end_date is NULL)
	
SELECT per_id, start_date, end_date
FROM unemployed NATURAL JOIN position NATURAL JOIN works
WHERE pos_code = 2200; 

--16. List the average, maximum and minimum annual pay (total salaries or wage rates multiplying by 1920 hours) of each industry (listed in GICS) in the order of the industry names.

WITH job_industry(comp_id, job_code, pos_code, ind_id, ind_title, pay_rate, pay_type) AS (SELECT sub_industry.comp_id, job.job_code, position.pos_code, ind_id, ind_title, pay_rate, pay_type 
	      										  FROM job, position, sub_industry natural join GICS 
											  WHERE job.pos_code = position.pos_code                     			
											  AND position.comp_id = sub_industry.comp_id)

SELECT ind_id, max(income), min(income), avg(income), ind_title
FROM ((SELECT (pay_rate * 1920) AS income, ind_id, ind_title
       	       FROM job_industry
       	       WHERE pay_type = �wage�)
       	       UNION
      	       (SELECT pay_rate AS income, ind_id, ind_title
       		FROM job_industry
       		WHERE pay_type = �salary�
		GROUP BY ind_id, ind_title
       		ORDER BY ind_title);

--17. Find out the biggest employer, industry, and industry group in terms of number of employees. (Three queries)

SELECT comp_id, max(num_emp) AS biggest_employer	
FROM(SELECT comp_id, COUNT(per_id) AS num_emp
    	     FROM position NATURAL JOIN works
    	     WHERE end_date IS NULL 
    	     GROUP BY comp_id);	

SELECT ind_id, MAX(num_of_emp) AS biggest_industry
FROM (SELECT ind_id, count(per_id) as num_of_emp
      	      FROM sub_industry NATURAL JOIN works NATURAL JOIN position
      	      WHERE sub_industry.comp_id = position.comp_id
      	 	    AND end_date is NULL
	      GROUP BY ind_id);

WITH biggest_industry_group AS (SELECT industry_group, COUNT(per_id) AS num_of_emp
 			         	FROM company NATURAL JOIN position NATURAL JOIN works
 			         	WHERE end_date is NULL
 			         	GROUP BY  industry_group)

SELECT industry_group, MAX(num_of_emp)
FROM biggest_industry_group;

--18. Find out the job distribution among industries by showing the number of employees in each industry.
	

SELECT industry_group ,comp_id, comp_name, COUNT(per_id) AS num_emp  
FROM works NATURAL JOIN position NATURAL JOIN company
WHERE end_date IS NULL
GROUP_BY industry_group;

--19. Given a person�s identifier and a pos code, find the courses (course id and title) that each alone teaches all the missing skills for this person to be qualified for the specified position, assuming the skill gap of the worker and the requirement of the position can be covered by one course.

WITH course_needed AS (SELECT sk_code 
			       FROM requires
			       WHERE pos_code = 1100
			       MINUS
			       SELECT sk_code
			       FROM has_skill
			       WHERE per_id = 1)

SELECT c_code,c_title
FROM course 
WHERE NOT EXISTS (SELECT sk_code
			  FROM course_needed
			  MINUS
		          SELECT teaches.sk_code                                               
		          FROM teaches 
		          WHERE course.c_code = teaches.c_code);

--20. Given a person�s identifier, find the job position with the highest pay rate for this person according to his/her skill possession.

SELECT DISTINCT pos_code, pay_range_high
FROM position AS p
WHERE NOT EXISTS (SELECT sk_code
			  FROM has_skill
			  WHERE per_id = 2
			  MINUS 
			  SELECT sk_code
			  FROM requires AS r
			  WHERE r.pos_code = p.pos_code);

--21. Given a position code, list all the names along with the emails of the persons who are qualified for this position.

SELECT per_name, email
FROM person AS P
WHERE NOT EXISTS (SELECT sk_code
			  FROM requires
			  WHERE pos_code 1100
			  MINUS
			  SELECT sk_code
			  FROM has_skill AS hs
			  WHERE hs.per_id = p.per_id);


--22. When a company cannot find any qualified person for a job position, a secondary solution is to find a person who is almost qualified to the job position. Make a �missing-k� list that lists people who miss only k skills for a specified pos code; k < 4.

WITH qualified_people_skills (per_id, skill_count) AS (SELECT per_id, COUNT(sk_code)
						       	       FROM has_skill NATURAL JOIN requires
		   				               WHERE pos_code = 1100							                              							       GROUP BY per_id)
SELECT per_id
FROM qualified_people_skills
WHERE skill_count >=(SELECT COUNT(*)-2
		    	     FROM requires
		    	     WHERE pos_code = 1100); 

--23. Suppose there is a new position that has nobody qualified. List the persons who miss the least number of skills that are required by this pos code and report the �least number�.

WITH qualified(per_id, sk_code) AS (SELECT per_id, pos_code, sk_code													    FROM person, requires														    WHERE pos_code = 1100
					    MINUS
	  				    SELECT per_id, sk_code
					    FROM has_skill),

	counted AS (SELECT COUNT(sk_code) AS missing_count, per_id
	            FROM qualified
	            GROUP BY per_ID)

SELECT per_id, MIN(missing_count) as least_missing_skills
FROM qualified NATURAL JOIN counted
GROUP BY per_id;

--24. List each of the skill code and the number of people who misses the skill and are in the missing-k list for a given position code in the ascending order of the people counts

WITH missing AS (SELECT per_id, sk_code 
		 	 FROM person, requires
		 	 WHERE pos_code = 1100 
		   	 MINUS
 	         	 SELECT per_id, sk_code 
  	         	 FROM has_skill),
	
	     counted AS (SELECT COUNT(sk_code) AS missing_count, per_ID 
	          	 FROM missing
	                 GROUP BY per_id)

SELECT sk_code, COUNT(per_id) as counted_people
FROM missing NATURAL JOIN counted
GROUP BY sk_code
ORDER BY counted_people ASC;

--25. Find out the number of the workers whose earnings increased in a speci?c industry group (use attribute �industry group� in table Company). [Hint: earning change = the sum of a person�s current earnings � the pay of the person�s the last previous job.]   

WITH current_salary(per_id, job_code, pos_code, current_annual_pay) AS (SELECT per_id, job_code, pos_code, 
                                               			        CASE
                                                			WHEN pay_typer = 'wage'
                                                			THEN pay_rate * 1970
                                                			ELSE pay_rate
                                                			END AS current_annual_pay
FROM job natural join works natural join (SELECT per_id, end_date                                
                                          FROM works 
                                          WHERE end_date is NULL)),
                        
	previous_salary AS (SELECT per_id, job_code, pos_code, 
                    CASE
                    WHEN pay_typer = 'wage'
                    THEN pay_rate * 1970
                    ELSE pay_rate
                    END AS previous_annual_pay
FROM job natural join works natural join (SELECT per_id, end_date                                
                                          FROM works 
                                          WHERE end_date IS NOT NULL)
                                          GROUP BY per_id), 

	job_industry AS (SELECT industry_group, comp_id, pos_code
		 FROM company natural join position),

	pay_increase AS (select COUNT() as increased_pay
                 FROM current_salary natural join previous_salary
                 where current_annual_pay > previous_annual_pay)

SELECT industry_group, increased_pay
FROM job_industry NATURAL JOIN pay_increase
GROUP BY industry_group

--26. Find the position that have the most openings due to lack of qualified workers. If there are many openings of a position but at the same time there are many qualified jobless people. Then training cannot help fill up this type of job vacancies. What we want to find is the position that has the largest difference between vacancies (the unfilled jobs) and the number of jobless people who are qualified for the position.


WITH unfilled_jobs AS (SELECT pos_code,job_code
			FROM position
			MINUS
			SELECT pos_code,job_code
			FROM works NATURAL JOIN position
			WHERE end_date IS NULL),

	    countUnfilledJobs AS (SELECT job_code, COUNT(pos_code) as Vacant
				   FROM unfilled_jobs
			           GROUP BY job_code),

	     Qualified_worker AS ( SELECT job_code, COUNT(per_id) AS qualified
				   FROM countUnfilledjobs,person
		  		   WHERE NOT EXISTS (SELECT sk_code
				  		     FROM countUnfilledjobs NATURAL JOIN required_by_job
				  		     MINUS
				  		     SELECT sk_code
				  		     FROM has_skill)),

	     Job_difference AS (SELECT job_code, (Vacant - qualified) AS difference
		   		FROM countUnfilledJobs NATURAL JOIN Qualified_worker),

SELECT job_code, MAX(difference)
FROM Job_difference; 

