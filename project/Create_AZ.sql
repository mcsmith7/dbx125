CREATE TABLE Store (
store_id 	NUMERIC,
address		VARCHAR (30),
zip_Code	NUMERIC(5),
phone		NUMERIC(10),
PRIMARY KEY (store_id));

CREATE TABLE Inventory (
item_Num	NUMERIC (10),
sh_title	VARCHAR (10),
title		VARCHAR (10),		
description 	VARCHAR (50),
quantity 	NUMERIC (3),
unit 		NUMERIC (5),
Avg_cost	NUMERIC (7),
Old_date	VARCHAR (10),
Min_leval	VARCHAT (10),
Shelf_code 	NUMERIC (10),
	PRIMARY KEY ( item_num ) 
);



CREATE TABLE sales (
Invoice_num 		NUMERIC (10),
Date 			VARCHAR (10),
Item_num 		NUMERIC (10),
Quantity 		NUMERIC (10),
Unit_cost 		NUMERIC (8),
Note 			VARCHAR (50),
	PRIMARY KEY (Invoice_num ),
	FOREIGN KEY (item_Num) REFERENCES  Inventory(item_Num)
);

CREATE TABLE purchase (
Pur_num 		NUMERIC (10),
Date 			VARCHAR (10),
Item_num 		NUMERIC (10),
Quantity 		NUMERIC (10),
Unit_cost		NUMERIC (8),
Note 			VARCHAR (50),
	PRIMARY KEY (pur_num)
);


CREATE TABLE account_payable (
Sup_id 		VARCHAR (5),
Balance 	NUMERIC (10),
	PRIMARY KEY (sup_id)
);


CREATE TABLE PurchasePaymentRecord (
Sup_id 		VARCHAR (5),
Pur_num 	NUMERIC (5),
Date 		VARCHAR (10),
Amount 		NUMERIC (10),
	PRIMARY KEY (Pur_num),
	FOREIGN KEY (Sup_id) REFERENCES account_payable(Sup_id)
);

CREATE TABLE person(
    per_ID         	NUMERIC,
    per_firstname       VARCHAR(70) NOT NULL,
    per_lastname	VARCHAR(70) NOT NULL,
    per_street      	VARCHAR(50),
    per_city        	VARCHAR(50),
    per_state       	VARCHAR(3),
    per_zip_code   	NUMERIC,
    per_gender		VARCHAR(10),		
    email           	VARCHAR(200),
    PRIMARY KEY(per_ID)
);

CREATE TABLE job (
    job_code		NUMERIC,
    pos_code		NUMERIC,
    emp_mode		VARCHAR(10),
    Pay_rate		NUMERIC(6,2),
    pay_type		VARCHAR(15),
    cate_code	        VARCHAR,
    Store_ID		NUMERIC,
    PRIMARY KEY(job_code), 
    FOREIGN KEY(pos_code) REFERENCES position,
    FOREIGN KEY(Store_Id) REFERENCES Store
);









CREATE TABLE position(
    pos_code        	NUMERIC,
    pos_title		VARCHAR(50),
    pos_description	VARCHAR(200),
    emp_mode        	VARCHAR(60),
    store_id         	NUMERIC,
    pay_range_high	NUMERIC,
    pay_range_low	NUMERIC,
    job_code		NUMERIC,
    PRIMARY KEY(pos_code)
    FOREIGN KEY(store_id) REFERENCES store
);

  CREATE TABLE phone(
    per_ID      		NUMERIC,
    mobile_num     	 	VARCHAR(20),
    home_num			NUMERIC,
    work_num			NUMERIC,		
    PRIMARY KEY(per_ID, mobile_num),
    FOREIGN KEY(per_ID) REFERENCES person
);

CREATE TABLE works(
    per_ID          		NUMERIC,
    job_code       		NUMERIC,
    start_date      		DATE NOT NULL,
    end_date       		DATE,
    PRIMARY KEY(job_code, per_ID)
    FOREIGN KEY(per_ID) REFERENCES person

);










CREATE TABLE section(
    c_code          	NUMERIC,
    sec_no          	NUMERIC,
    complete_date   	DATE,
    sec_year        	NUMERIC(4),
    offered_by      	VARCHAR(60),
    format      	VARCHAR(60),
    price           	NUMERIC,
    PRIMARY KEY(c_code, sec_no)
    FOREIGN KEY(c_code) REFERENCES Course
);

CREATE TABLE course(
    c_code              NUMERIC,
    title               VARCHAR(80),
    level               VARCHAR(20),
    description         VARCHAR(300),
    status              VARCHAR(80),
    retail_price	NUMERIC(4,2),
    PRIMARY KEY(c_code)
);

CREATE TABLE has_skill(
    per_ID      NUMERIC,
    sk_code     VARCHAR(10),
    PRIMARY KEY(per_ID, sk_code),
    FOREIGN KEY(per_ID) REFERENCES person
);

CREATE TABLE skill(
   sk_code             NUMERIC,
   sk_title            VARCHAR(60),
   sk_description      VARCHAR(300),   		
   level	       VARCHAR(20),
   PRIMARY KEY(sk_code)
);

CREATE TABLE teaches(
    c_code      		NUMERIC,
    sk_code     		NUMERIC,
    PRIMARY KEY(c_code, sk_code)
    FOREIGN KEY(sk_code) REFERENCES skill
);



CREATE TABLE prerequisite(
    c_code          		NUMERIC,
    required_code   		NUMERIC,
   PRIMARY KEY(c_code, required_code)
   FOREIGN KEY(c_code)REFERENCES course
);

CREATE TABLE requires(
    pos_code    		NUMERIC,
    sk_code     		NUMERIC,
    PRIMARY KEY(pos_code,sk_code),
    FOREIGN KEY(sk_code) REFERENCES skill
    FOREIGN KEY(pos_code) REFERENCES position
);




CREATE TABLE required_by_job(
    job_code    		NUMERIC,
    sk_code     		NUMERIC,
    PRIMARY KEY(job_code,sk_code),
    FOREIGN KEY(sk_code) REFERENCES skill
    FOREIGN KEY(job_code) REFERENCES job
);

CREATE TABLE takes (
    per_ID               NUMERIC,
    c_code               NUMERIC,
    sec_no               NUMERIC,
    complete_date   	 DATE,
    FOREIGN KEY(per_ID) REFERENCES person,
    FOREIGN KEY(c_code, sec_no) REFERENCES section
);
