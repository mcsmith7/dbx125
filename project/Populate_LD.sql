--Insert values to Person
--person(per_ID, per_name, per_street, per_city, per_state, per_zip_code, email, per_gender)
INSERT INTO person VALUES (1, 'Elena Leonard', '9729 Pilgrim Lane', 'Taylors', 'SC', 29687, 'eleonard@gmail.com', 'female');
INSERT INTO person VALUES (2, 'Francis King', '9881 Brandywine Rd', 'Altoona', 'PA', 16601, 'francisking@gmail.com','male');
INSERT INTO person VALUES (3, 'Anna Holt', '42 Brookside Ave.', 'Garland', 'TX', 75043, 'annaholt@outlook.com', 'female');
INSERT INTO person VALUES (4, 'Daniel Becker', '7092 Madison Road', 'Westwood', 'NJ', 07675, 'danielbecker@yahoo.com','male');
INSERT INTO person VALUES (5, 'Ron Williams', '8679 La Sierra Ave.', 'Tucson', ' AZ', 85718, 'rwilliams@yahoo.com','male');

--company(comp_ID, comp_name, comp_street, comp_city, comp_state, comp_zip_code, industry_group, website, pos_code)
--I filled up industry group number with numbers 100 - 500 count by 100
INSERT INTO company VALUES (1000, 'Googleplex', '1600 Amphitheatre Parkway', 'Mountainview', 'CA', 94043, 100, 'google.com');
INSERT INTO company VALUES (2000, 'BMW', '300 Chestnut Ridge Rd', 'Woodcliff Lake','NJ', 07677, 200, 'www.bmwusa.com');
INSERT INTO company VALUES (3000, 'Facebook', '1 Hacker Way','Menlo Park', 'CA', 94025, 300, 'www.Facebook.com');
INSERT INTO company VALUES (4000, 'Keystone Automotive', '5620 Blessey St','Harahan','LA', 70123, 400,'www.keystoneautomotive.com');
INSERT INTO company VALUES (5000, 'Autozone', '123 S. Front St', 'Memphis', 'TN', 38103, 500, 'www.Autozone.com');

--position(pos_code, pos_title, pos_description, emp_mode, comp_ID, pay_range_high, pay_range_low)
INSERT INTO position VALUES (1100, 'Database Models','Create and manage database','Full-Time', 1000, 60000, 30000,10);
INSERT INTO position VALUES (2200, 'Application Development','Front and Back end application development', 'Part-time',1000, 45000, 35000,20);
INSERT INTO position VALUES (3300, 'Automotive Test Engineer','defines innovation and excellence in signal processing', 'Part-time', 2000,50000, 35000,30);
INSERT INTO position VALUES (4400, 'Controls Specialists','control checklist to assure all aspects of detailing', 'Full-time', 2000, 40000, 60000, 40);
INSERT INTO position VALUES (5500, 'IT project manager','In-depth technical ability and knowledge of Data Center', 'Full-time', 3000, 80000, 20000,50);
INSERT INTO position VALUES (6600, 'Creative Coder','Use javaScript through multiple iterations', 'Full-time', 3000, 80000, 60000,60);
INSERT INTO position VALUES (7700, 'Detail shop','Conduct maintenance for assigned equipment', 'Part-time', 4000, 20000, 8000, 70);
INSERT INTO position VALUES (8800, 'Mechanic', 'performs service and installation on customer vehicles', 'Full-time', 4000, 45000, 30000,80);
INSERT INTO position VALUES (9900, 'Retail Sales Associates', 'sales through superior customer service','Part-time',5000, 20000, 9000,90);
INSERT INTO position VALUES (2000, 'Auto Parts Delivery Driver', 'assure safe delivery of parts', 'Full-time', 5000, 45000, 20000,100);

--Skill(sk_code, sk_title, sk_description, level)
INSERT INTO skill VALUES (0001,'Java application skill', 'Ability to use java application', 'Beginner');
INSERT INTO skill VALUES (0002,'Linear Algebra', 'Ability to handle linear algebra problems', 'Advanced');
INSERT INTO skill VALUES (0003,'Database skill', 'Ability to handle databases', 'Medium');
INSERT INTO skill VALUES (0004,'Electrical Engineering skill', 'Ability to understand the electrical engineering behind cars', 'Advanced');
INSERT INTO skill VALUES (0005,'Automotive', 'knowledge about automotive', 'Beginner');
INSERT INTO skill VALUES (0006,'Mechanic skill', 'Excellent knowledge about car mechanics', 'Medium');
INSERT INTO skill VALUES (0007,'Leadership', 'Good leadership', 'Advanced');
INSERT INTO skill VALUES (0008,'Communication Skill','Ability to communicate well with the customers and others', 'Medium');
INSERT INTO skill VALUES (0009,'Python', 'Ability to use python well', 'Medium');
INSERT INTO skill VALUES (0010,'C', 'Ability to use C', 'Beginner');
INSERT INTO skill VALUES (0011,'Writing skill', 'Ability to communicate through writing, also use for writing documents', 'Medium');
INSERT INTO skill VALUES (0012,'Critical Thinking skill', 'Ability to solve hard problems', 'Advanced');
INSERT INTO skill VALUES (0013,'Problem Solving skill', 'Good at problem solving', 'Beginner');
INSERT INTO skill VALUES (0014,'Creative', 'Ability to be creative at work and making designs', 'Beginner');
INSERT INTO skill VALUES (0015,'Analytical skill', 'Good at Analytical thinking', 'Advanced');
INSERT INTO skill VALUES (0016,'Physics skill', 'Ability to use physics', 'Medium');
INSERT INTO skill VALUES (0017,'Presenting skill', 'Good at presenting', 'Beginner');
INSERT INTO skill VALUES (0018,'Organization skill', 'Ability to be organized at workplace', 'Beginner');
INSERT INTO skill VALUES (0019,'English skill', 'Ability to be able to use English Language well', 'Beginner');
INSERT INTO skill VALUES (0020,'Bilingual', 'Ability to use different language other than english', 'Medium');
INSERT INTO skill VALUES (0021,'Driving skill', 'Good at driving large vehicle', 'Advanced');
INSERT INTO skill VALUES (0022,'Flexible', 'Good at adapting in to any kind of environment', 'Beginner');
INSERT INTO skill VALUES (0023,'Money Handling skill', 'Ability to handle money well ', 'Beginner');
INSERT INTO skill VALUES (0024,'Customer service', 'Ability to handle customer well', 'Advanced');
INSERT INTO skill VALUES (0025,'Attentive', 'Ability to pay attention to details', 'Medium');
INSERT INTO skill VALUES (0026,'Disciplined', 'THIS WORKS FOR DRIVERS. ability to stay on track ', 'Medium');
INSERT INTO skill VALUES (0027,'Time Management skill', 'ability to stay on time', 'Beginner');
INSERT INTO skill VALUES (0028,'Reading Skill', 'good at understanding documents', 'Beginner');
INSERT INTO skill VALUES (0029,'Machine Learning', 'Good at understanding machines', 'Advanced');
INSERT INTO skill VALUES (0030,'Mapping Skill', 'Good at navigating directions', 'Medium');


--has_skill(per_ID, sk_code)
INSERT INTO has_skill VALUES (1,0001);
INSERT INTO has_skill VALUES (1,0008);
INSERT INTO has_skill VALUES (1,0013);
INSERT INTO has_skill VALUES (2,0001);
INSERT INTO has_skill VALUES (2,0009);
INSERT INTO has_skill VALUES (2,0013);
INSERT INTO has_skill VALUES (2,0010);
INSERT INTO has_skill VALUES (2,0017);
INSERT INTO has_skill VALUES (3,0023);
INSERT INTO has_skill VALUES (3,0020);
INSERT INTO has_skill VALUES (3,0022);
INSERT INTO has_skill VALUES (3,0007);
INSERT INTO has_skill VALUES (4,0004);
INSERT INTO has_skill VALUES (4,0005);
INSERT INTO has_skill VALUES (4,0007);
INSERT INTO has_skill VALUES (4,0017);
INSERT INTO has_skill VALUES (4,0027);
INSERT INTO has_skill VALUES (5,0029);
INSERT INTO has_skill VALUES (5,0021);


--requires(pos_code, sk_code)
INSERT INTO requires VALUES (1100,0001);
INSERT INTO requires VALUES (1100,0003);
INSERT INTO requires VALUES (1100,0008);
INSERT INTO requires VALUES (2200,0001);
INSERT INTO requires VALUES (2200,0013);
INSERT INTO requires VALUES (3300,0005);
INSERT INTO requires VALUES (3300,0006);
INSERT INTO requires VALUES (4400,0006);
INSERT INTO requires VALUES (4400,0016);
INSERT INTO requires VALUES (5500,0006);
INSERT INTO requires VALUES (5500,0007);
INSERT INTO requires VALUES (5500,0015);
INSERT INTO requires VALUES (5500,0010);
INSERT INTO requires VALUES (6600,0001);
INSERT INTO requires VALUES (6600,0014);
INSERT INTO requires VALUES (6600,0019);
INSERT INTO requires VALUES (6600,0025);
INSERT INTO requires VALUES (7700,0024);
INSERT INTO requires VALUES (7700,0025);
INSERT INTO requires VALUES (7700,0008);
INSERT INTO requires VALUES (8800,0005);
INSERT INTO requires VALUES (8800,0015);
INSERT INTO requires VALUES (8800,0024);
INSERT INTO requires VALUES (8800,0008);
INSERT INTO requires VALUES (9900,0024);
INSERT INTO requires VALUES (9900,0008);
INSERT INTO requires VALUES (9900,0019);
INSERT INTO requires VALUES (2000,0030);
INSERT INTO requires VALUES (2000,0027);
INSERT INTO requires VALUES (2000,0026);
INSERT INTO requires VALUES (2000,0021);

--Job(job_code, pos_code, emp_mode, pay_rate, pay_type, cate_code)
INSERT INTO job VALUES (10,1100,'Full-Time',40000.00,'Salary','Medium');
INSERT INTO job VALUES (20,2200,'Part-Time',25.00,'Wage','Medium');
INSERT INTO job VALUES (30,3300,'Part-Time',30.00,'Wage','High');
INSERT INTO job VALUES (40,4400,'Full-Time',45000.00,'Salary', 'High');
INSERT INTO job VALUES (50,5500,'Full-Time',50000.00,'Salary', 'High');
INSERT INTO job VALUES (60,6600,'Full-Time',60000.00,'Salary', 'Medium');
INSERT INTO job VALUES (70,7700,'Part-Time',19.00,'Wage', 'Medium');
INSERT INTO job VALUES (80,8800,'Full-Time',30.00,'Wage', 'Medium');
INSERT INTO job VALUES (90,9900,'Part-Time',16.00,'Wage', 'Low');
INSERT INTO job VALUES (100,2000,'Full-Time',25.00,'Wage', 'Low');


--required_by_job(job_code, sk_code)
INSERT INTO required_by_job VALUES (10,0001);
INSERT INTO required_by_job VALUES (10,0003);
INSERT INTO required_by_job VALUES (20,0001);
INSERT INTO required_by_job VALUES (30,0006);
INSERT INTO required_by_job VALUES (40,0016);
INSERT INTO required_by_job VALUES (50,0010);
INSERT INTO required_by_job VALUES (50,0015);
INSERT INTO required_by_job VALUES (60,0001);
INSERT INTO required_by_job VALUES (70,0024);
INSERT INTO required_by_job VALUES (80,0024);
INSERT INTO required_by_job VALUES (80,0015);
INSERT INTO required_by_job VALUES (90,0008);
INSERT INTO required_by_job VALUES (100,0021);
INSERT INTO required_by_job VALUES (100,0030);

--phone(per_ID, mobile_num, home_num, work_num)
INSERT INTO phone VALUES (1,'803-201-4815', '803-200-3273', '');
INSERT INTO phone VALUES (2,'814-204-1927', '814-201-9499', '');
INSERT INTO phone VALUES (3,'972-240-8926', '972-205-8848', '');
INSERT INTO phone VALUES (4,'201-383-4112', '201-534-2652', '');
INSERT INTO phone VALUES (5,'520-229-6647', '520-230-3559', '');

--works(per_ID, job_code, start_date, end_date)
INSERT INTO works VALUES (1, 10, '30-MAR-16', '04-JUN-18');
INSERT INTO works VALUES (1, 20, '12-JULY-18','16-SEP-20');
INSERT INTO works VALUES (1, 50, '09-OCT-20',NULL);
INSERT INTO works VALUES (2, 20, '21-FEB-18', '12-SEP-19');
INSERT INTO works VALUES (3, 30, '20-MAY-19', NULL);
INSERT INTO works VALUES (4, 40, '12-MAR-12', '29-DEC-18');
INSERT INTO works VALUES (4, 50, '05-JAN-19', NULL);
INSERT INTO works VALUES (5, 50, '09-AUG-17', NULL);




--course(c_code, c_title, level, c_description, status ,retail_price)
INSERT INTO course VALUES ('LIT824','Technical Writing','intermediate','Teaches principles of writing for business','active','$187.86');
INSERT INTO course VALUES ('CS721','Data Models','Advanced','Teaches skills in information system and database application','expired','$365.58');
INSERT INTO course VALUES ('BS370','Business management','Advanced','Teaches skills needed for effective leadership','expired','$215.98');
INSERT INTO course VALUES ('MCH182','Automotive Engineering','intermediate','Learn skills required to repair machinery', 'expired','$450.67');
INSERT INTO course VALUES ('CS575','C Programming','intermediate','Teaches fundamentals of c programming language and design concepts','active','$643.96');
INSERT INTO course VALUES ('CS496','Java Programming', 'intermediate','Teaches fundamentals of the java programming language and design concepts','active','$647.78');
INSERT INTO course VALUES ('MCH088','CDL Training','Beginner',"Provides training needed to obtain a Commercial Drivers License",'active','$376.23');
INSERT INTO course VALUES ('MATH438','Applied Mathematics','Advanced',"Teaches advanced mathematics and physics",'active','$605.14');
INSERT INTO course VALUES ('LIT477','English I','Beginner','Teaches basic english composition and pronunciation','active','$468.28');
INSERT INTO course VALUES ('CS633','Python','intermediate','Teaches fundamentals of the python programming language and design concepts','active','$464.31');
INSERT INTO course VALUES ('CS654','Machine Learning','intermediate','Learn design principles needed to program artificial intelligence.','active','$532.56'); 

--section(c_code, sec_no, complete_date, sec_year, offered_by, format, price)
INSERT INTO section VALUES ('LIT824',332,"10-Apr",2018,'UNO','correspondence','$187.86'); 
INSERT INTO section VALUES ('CS721',261,"1-Feb",2019,'Googleplex','classroom','$365.58'); 
INSERT INTO section VALUES ('BS370',335,"04-Dec",2018,'UNO','online self-paced','$215.98'); 
INSERT INTO section VALUES ('MCH182',320,"01-Apr",2018,'General Vehicle','online-sync','$450.67'); 
INSERT INTO section VALUES ('CS575',336,"25-May",2018,'Googleplex','classroom','$643.96'); 
INSERT INTO section VALUES ('CS496',192,"19-Sep",2018,'Googleplex','online self-paced','$647.78'); 
INSERT INTO section VALUES ('MCH008',187,"02-Mar",2019,'Acar Zone','classroom','$376.23'); 
INSERT INTO section VALUES ('MATH438',174,"08-Apr",2020,'UNO','online-sync','$605.14'); 
INSERT INTO section VALUES ('LIT477',380,"02-Jan",2019,'UNO','correspondence','$468.28'); 
INSERT INTO section VALUES ('CS633',190,"24-Dec",2018,'GooglePlex','correspondence','$464.31'); 
INSERT INTO section VALUES ('CS654',203,"16-Jul",2020,'GooglePLex','classroom','$532.56');

--teaches(c_code, sk_code)
INSERT INTO teaches VALUES ('LIT824',0012);
INSERT INTO teaches VALUES ('LIT824',0014);
INSERT INTO teaches VALUES ('LIT824',0008);
INSERT INTO teaches VALUES ('CS721',0003);
INSERT INTO teaches VALUES ('CS721',0015);
INSERT INTO teaches VALUES ('BS370',0007);
INSERT INTO teaches VALUES ('BS370',0008);
INSERT INTO teaches VALUES ('BS370',0017);
INSERT INTO teaches VALUES ('BS370',0018);
INSERT INTO teaches VALUES ('MCH182',0005);
INSERT INTO teaches VALUES ('MCH182',0006);
INSERT INTO teaches VALUES ('CS575',0010);
INSERT INTO teaches VALUES ('CS575',0012);
INSERT INTO teaches VALUES ('CS575',0013);
INSERT INTO teaches VALUES ('CS496',0001);
INSERT INTO teaches VALUES ('CS496',0012);
INSERT INTO teaches VALUES ('CS496',0013);
INSERT INTO teaches VALUES ('MCH008',0013);
INSERT INTO teaches VALUES ('MATH438',0002);
INSERT INTO teaches VALUES ('MATH438',0016);
INSERT INTO teaches VALUES ('MATH438',0013);
INSERT INTO teaches VALUES ('LIT477',0019);
INSERT INTO teaches VALUES ('CS633',0009);
INSERT INTO teaches VALUES ('CS654',0015);
INSERT INTO teaches VALUES ('CS654',0029);

--prerequisite(c_code, required_code)
INSERT INTO prerequisite VALUES ('CS721','CS496');
INSERT INTO prerequisite VALUES ('LIT824','LIT447');
INSERT INTO prerequisite VALUES ('CS575','CS496');
INSERT INTO prerequisite VALUES ('CS654','CS721');
INSERT INTO prerequisite VALUES ('CS654','MATH438');

--takes(per_id, c_code, sec_no, complete_date)
INSERT INTO takes VALUES (1,'CS721',261,'1-Feb-2019');
INSERT INTO takes VALUES (1,'LIT824',332,'10-Apr-2018');
INSERT INTO takes VALUES (2,'BS370',335,'04-Dec-2020');
INSERT INTO takes VALUES (2,'CS575',336,'25-May-2018');
INSERT INTO takes VALUES (4,'BS370',335,'04-Dec-2020');
INSERT INTO takes VALUES (4,'MCH182',320,'01-Apr-2018');
INSERT INTO takes VALUES (5,'CS654',203,'16-Jul-2020');

--sub_industy(ind_ID, comp_ID);
INSERT INTO sub_industry VALUES (45102020,1000);
INSERT INTO sub_industry VALUES (45102030,1000);
INSERT INTO sub_industry VALUES (25102010,2000);
INSERT INTO sub_industry VALUES (25102020,2000);
INSERT INTO sub_industry VALUES (50201010,3000);
INSERT INTO sub_industry VALUES (50203010,3000);
INSERT INTO sub_industry VALUES (25504050,4000);
INSERT INTO sub_industry VALUES (25504050,5000);

--GICS(ind_ID,ind_title,level,description,parent_ID)
INSERT INTO GICS VALUES (4510,'Software & Services','Industry Group','Companies that provide IT Services and Software',45);  
INSERT INTO GICS VALUES (451020,'IT Services','Industry','Providers of IT consulting,data processing,and internet services',4510);
INSERT INTO GICS VALUES (45102010,'IT Consulting & Other Services','Sub-Industry','Providers of information technology and systems integration services.',451020);
INSERT INTO GICS VALUES (45102020,'Data Processing & Outsourced Services','Sub-Industry','Providers of commercial electronic data processing and/or business process outsourcing services.',451020);
INSERT INTO GICS VALUES (45102030,'Internet Services & Infrastructure','Sub-Industry','Companies providing services and infrastructure for the internet industry',451020);
INSERT INTO GICS VALUES (451030,'Software','Industry','Providers of application and system software',4510);
INSERT INTO GICS VALUES (45103010,'Application Software','Sub-Industry','Companies engaged in developing and producing software designed for specialized applications for the business or consumer market.',451030);
INSERT INTO GICS VALUES (45103020,'Systems Software','Sub-Industry','Companies engaged in developing and producing systems and database management software.',451030);
INSERT INTO GICS VALUES (2510,'Automobiles & Components','Industry Group','Companies that manufacture,auto parts & equipment,tires and automobiles',25);
INSERT INTO GICS VALUES (251010,'Auto Components','Industry','Manufacturers of Automobile parts and tires',2510);
INSERT INTO GICS VALUES (25101010,'Auto Parts & Equipment','Sub-Industry','Manufacturers of parts and accessories for automobiles and motorcycles.',251010);
INSERT INTO GICS VALUES (25101020,'Tires & Rubber','Sub-Industry','Manufacturers of tires and rubber.',251010);
INSERT INTO GICS VALUES (251020,'Automobiles','Industry','Manufacturers of passenger cars and motorcycles',2510);
INSERT INTO GICS VALUES (25102010,'Automobile Manufacturers','Sub-Industry','Companies that produce mainly passenger automobiles and light trucks.',251020);
INSERT INTO GICS VALUES (25102020,'Motorcycle Manufacturers','Sub-industry','Companies that produce motorcycles,scooters or three-wheelers.',251020);
INSERT INTO GICS VALUES (2550,'Retailing','Industry Group','Distribution and sales',25);
INSERT INTO GICS VALUES (25501,'Distributors','Industry','Distributors and wholesalers of general merchandise not classified elsewhere.',2550);
INSERT INTO GICS VALUES (255040,'Specialty Retail','Industry','Providers of specialty goods,such as apparel,automobiles and electronics',2550);
INSERT INTO GICS VALUES (25504050,'Automotive Retail','Sub-Industry','Owners and operators of stores specializing in automotive retail.',255040);
INSERT INTO GICS VALUES (5020,'Media & Entertainment','Industry Group','Companies that provide digital entertainment as well as generate revenue through advertising.',20);
INSERT INTO GICS VALUES (502010,'Media','Industry','Providers of advertising and entertainment mediums.',5020);
INSERT INTO GICS VALUES (50201010,'Advertising','Sub-Industry','Companies providing advertising,marketing or public relations services.',502010);
INSERT INTO GICS VALUES (50203010,'Interactive Media & Services','Sub-Industry','Companies engaging in content and information creation or distribution through proprietary platforms.',502010); 


 