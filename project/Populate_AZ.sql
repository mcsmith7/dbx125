INSERT INTO INVENTORY VALUES ('0000000001', 'mflr', 'Muffler', '5 x 10 Muffler', '40', '152', '100', '04242022', '5', '8923100892');
INSERT INTO INVENTORY VALUES ('0000000002', 'Tire', 'Tire', 'Singular 22 inch tire', '50', '006', '40', '06222030', '10', '24246738');
INSERT INTO INVENTORY VALUES ('0000000003', 'trnsmtn', 'Transmition', 'transmition for 2010+ ford taures', '10', '100', '35', '10102031','30', '8610220410');
INSERT INTO INVENTORY VALUES ('0000000004','Win. Wipers', 'Winshield wipers', '20 Inch. Windshielf whipers', '100', '90', '15', '08292022','10', '9292010110');
INSERT INTO INVENTORY VALUES ('0000000005', 'Brk. pad', 'Break pads', '4 x 3 break pads', '4', '237','35', '01222025','5', '12560034871');


INSERT INTO SALES VALUES ('0000000123', '04212020', '0000000001', '5', '12', 'Purchases 5x mufflers');
INSERT INTO SALES VALUES ('0000062523', '07072018', '0000000001', '25', '12', 'Purchases 25x mufflers');
INSERT INTO SALES VALUES ('0005019854', '03012014', '0000000005', '30', '43', 'Purchase of 30 Brake pads');
INSERT INTO SALES VALUES ('0006255120', '06112020', '0000000003', '5', '500', 'Sale of Transmitions');


INSERT INTO PURCHASE VALUES('0000000001', '04152020', '00000000001', '10','20','Muffler purchased');
INSERT INTO PURCHASE VALUES('0000000002', '03092019', '00000000001', '04','44','Purchases made');
INSERT INTO PURCHASE VALUES('0000000003', '12252015', '00000000001', '20','15','Merry Christmas');
INSERT INTO PURCHASE VALUES('0000000004', '09022020', '00000000005', '1','33','Emerg. Purchase');
INSERT INTO PURCHASE VALUES('0000000005', '05272017', '00000000004', '100','1500','restocking');


INSERT INTO account_payable VALUES ('12323', '300000');
INSERT INTO account_payable VALUES ('52520', '150000');
INSERT INTO account_payable VALUES ('09163', '500');
INSERT INTO account_payable VALUES ('22222', '1000000');
INSERT INTO account_payable VALUES ('00053', '450000');


INSERT INTO PurchasePaymentRecord VALUES ('12323', '90909', '08222020', '300000');
INSERT INTO PurchasePaymentRecord VALUES ('52520', '36780', '14102018', '150000');
INSERT INTO PurchasePaymentRecord VALUES ('09163', '21456', '12252016', '500');
INSERT INTO PurchasePaymentRecord VALUES ('22222', '12345', '02212017', '1000000');
INSERT INTO PurchasePaymentRecord VALUES ('00053', '76543', '07012019', '450000');

INSERT INTO person VALUES (1, 'Elena', 'Leonard', '9729 Pilgrim Lane', 'Taylors', 'SC', 29687, 'eleonard@gmail.com', 'female');
INSERT INTO person VALUES (2, 'Francis','King', '9881 Brandywine Rd', 'Altoona', 'PA', 16601, 'francisking@gmail.com','male');
INSERT INTO person VALUES (3, 'Anna','Holt', '42 Brookside Ave.', 'Garland', 'TX', 75043, 'annaholt@outlook.com', 'female');
INSERT INTO person VALUES (4, 'Daniel','Becker', '7092 Madison Road', 'Westwood', 'NJ', 07675, 'danielbecker@yahoo.com','male');
INSERT INTO person VALUES (5, 'Ron','Williams', '8679 La Sierra Ave.', 'Tucson', ' AZ', 85718, 'rwilliams@yahoo.com','male');


INSERT INTO store VALUES ('00001', '245 Jackson St.', '65432', '2357864343');
INSERT INTO store VALUES ('00002', '578 Newton Ave.', '78932', '8902346754');
INSERT INTO store VALUES ('00003', '890 Corman Rd.', '98032', '5437896352');
INSERT INTO store VALUES ('00004', '7843 Hasfin Dr.', '70032', '5048973434');
INSERT INTO store VALUES ('00005', '324 Kipmen St.', '89032', '9083437865');

INSERT INTO skill VALUES (0001,'Java application skill', 'Ability to use java application', 'Beginner');
INSERT INTO skill VALUES (0002,'Linear Algebra', 'Ability to handle linear algebra problems', 'Advanced');
INSERT INTO skill VALUES (0003,'Database skill', 'Ability to handle databases', 'Medium');
INSERT INTO skill VALUES (0004,'Electrical Engineering skill', 'Ability to understand the electrical engineering behind cars', 'Advanced');
INSERT INTO skill VALUES (0005,'Automotive', 'knowledge about automotive', 'Beginner');
INSERT INTO skill VALUES (0006,'Mechanic skill', 'Excellent knowledge about car mechanics', 'Medium');
INSERT INTO skill VALUES (0007,'Leadership', 'Good leadership', 'Advanced');
INSERT INTO skill VALUES (0008,'Communication Skill','Ability to communicate well with the customers and others', 'Medium');
INSERT INTO skill VALUES (0009,'Python', 'Ability to use python well', 'Medium');
INSERT INTO skill VALUES (0010,'C', 'Ability to use C', 'Beginner');
INSERT INTO skill VALUES (0011,'Writing skill', 'Ability to communicate through writing, also use for writing documents', 'Medium');
INSERT INTO skill VALUES (0012,'Critical Thinking skill', 'Ability to solve hard problems', 'Advanced');
INSERT INTO skill VALUES (0013,'Problem Solving skill', 'Good at problem solving', 'Beginner');
INSERT INTO skill VALUES (0014,'Creative', 'Ability to be creative at work and making designs', 'Beginner');
INSERT INTO skill VALUES (0015,'Analytical skill', 'Good at Analytical thinking', 'Advanced');
INSERT INTO skill VALUES (0016,'Physics skill', 'Ability to use physics', 'Medium');
INSERT INTO skill VALUES (0017,'Presenting skill', 'Good at presenting', 'Beginner');
INSERT INTO skill VALUES (0018,'Organization skill', 'Ability to be organized at workplace', 'Beginner');
INSERT INTO skill VALUES (0019,'English skill', 'Ability to be able to use English Language well', 'Beginner');
INSERT INTO skill VALUES (0020,'Bilingual', 'Ability to use different language other than english', 'Medium');
INSERT INTO skill VALUES (0021,'Driving skill', 'Good at driving large vehicle', 'Advanced');
INSERT INTO skill VALUES (0022,'Flexible', 'Good at adapting in to any kind of environment', 'Beginner');
INSERT INTO skill VALUES (0023,'Money Handling skill', 'Ability to handle money well ', 'Beginner');
INSERT INTO skill VALUES (0024,'Customer service', 'Ability to handle customer well', 'Advanced');
INSERT INTO skill VALUES (0025,'Attentive', 'Ability to pay attention to details', 'Medium');
INSERT INTO skill VALUES (0026,'Disciplined', 'THIS WORKS FOR DRIVERS. ability to stay on track ', 'Medium');
INSERT INTO skill VALUES (0027,'Time Management skill', 'ability to stay on time', 'Beginner');
INSERT INTO skill VALUES (0028,'Reading Skill', 'good at understanding documents', 'Beginner');
INSERT INTO skill VALUES (0029,'Machine Learning', 'Good at understanding machines', 'Advanced');
INSERT INTO skill VALUES (0030,'Mapping Skill', 'Good at navigating directions', 'Medium');


INSERT INTO position VALUES (1100, 'Database Models','Create and manage database','Full-Time', 00001, 60000, 30000,10);
INSERT INTO position VALUES (2200, 'Application Development','Front and Back end application development', 'Part-time',00002, 45000, 35000,20);
INSERT INTO position VALUES (3300, 'Automotive Test Engineer','defines innovation and excellence in signal processing', 'Part-time', 00003,50000, 35000,30);
INSERT INTO position VALUES (4400, 'Controls Specialists','control checklist to assure all aspects of detailing', 'Full-time', 00004, 40000, 60000, 40);
INSERT INTO position VALUES (5500, 'IT project manager','In-depth technical ability and knowledge of Data Center', 'Full-time', 00005, 80000, 20000,50);
INSERT INTO position VALUES (6600, 'Creative Coder','Use javaScript through multiple iterations', 'Full-time', 00001, 80000, 60000,60);
INSERT INTO position VALUES (7700, 'Detail shop','Conduct maintenance for assigned equipment', 'Part-time', 00002, 20000, 8000, 70);
INSERT INTO position VALUES (8800, 'Mechanic', 'performs service and installation on customer vehicles', 'Full-time', 00003, 45000, 30000,80);
INSERT INTO position VALUES (9900, 'Retail Sales Associates', 'sales through superior customer service','Part-time',00004, 20000, 9000,90);
INSERT INTO position VALUES (2000, 'Auto Parts Delivery Driver', 'assure safe delivery of parts', 'Full-time', 00005, 45000, 20000,100);

INSERT INTO has_skill VALUES (1,0001);
INSERT INTO has_skill VALUES (1,0008);
INSERT INTO has_skill VALUES (1,0013);
INSERT INTO has_skill VALUES (2,0009);
INSERT INTO has_skill VALUES (2,0013);
INSERT INTO has_skill VALUES (2,0010);
INSERT INTO has_skill VALUES (2,0017);
INSERT INTO has_skill VALUES (3,0023);
INSERT INTO has_skill VALUES (3,0020);
INSERT INTO has_skill VALUES (3,0022);
INSERT INTO has_skill VALUES (3,0007);
INSERT INTO has_skill VALUES (4,0004);
INSERT INTO has_skill VALUES (4,0005);
INSERT INTO has_skill VALUES (4,0007);
INSERT INTO has_skill VALUES (4,0017);
INSERT INTO has_skill VALUES (4,0027);
INSERT INTO has_skill VALUES (5,0029);
INSERT INTO has_skill VALUES (5,0021);

INSERT INTO requires VALUES (1100,0001);
INSERT INTO requires VALUES (1100,0003);
INSERT INTO requires VALUES (1100,0008);
INSERT INTO requires VALUES (2200,0001);
INSERT INTO requires VALUES (2200,0013);
INSERT INTO requires VALUES (3300,0005);
INSERT INTO requires VALUES (3300,0006);
INSERT INTO requires VALUES (4400,0006);
INSERT INTO requires VALUES (4400,0016);
INSERT INTO requires VALUES (5500,0006);
INSERT INTO requires VALUES (5500,0007);
INSERT INTO requires VALUES (5500,0015);
INSERT INTO requires VALUES (5500,0010);
INSERT INTO requires VALUES (6600,0001);
INSERT INTO requires VALUES (6600,0014);
INSERT INTO requires VALUES (6600,0019);
INSERT INTO requires VALUES (6600,0025);
INSERT INTO requires VALUES (7700,0024);
INSERT INTO requires VALUES (7700,0025);
INSERT INTO requires VALUES (7700,0008);
INSERT INTO requires VALUES (8800,0005);
INSERT INTO requires VALUES (8800,0015);
INSERT INTO requires VALUES (8800,0024);
INSERT INTO requires VALUES (8800,0008);
INSERT INTO requires VALUES (9900,0024);
INSERT INTO requires VALUES (9900,0008);
INSERT INTO requires VALUES (9900,0019);
INSERT INTO requires VALUES (2000,0030);
INSERT INTO requires VALUES (2000,0027);
INSERT INTO requires VALUES (2000,0026);
INSERT INTO requires VALUES (2000,0021);

INSERT INTO job VALUES (10,1100,'Full-Time',40000.00,'Salary','Medium',1);
INSERT INTO job VALUES (20,2200,'Part-Time',25.00,'Wage','Medium',2);
INSERT INTO job VALUES (30,3300,'Part-Time',30.00,'Wage','High',3);
INSERT INTO job VALUES (40,4400,'Full-Time',45000.00,'Salary', 'High',4);
INSERT INTO job VALUES (50,5500,'Full-Time',50000.00,'Salary', 'High',5);
INSERT INTO job VALUES (60,6600,'Full-Time',60000.00,'Salary', 'Medium',1);
INSERT INTO job VALUES (70,7700,'Part-Time',19.00,'Wage', 'Medium',2);
INSERT INTO job VALUES (80,8800,'Full-Time',30.00,'Wage', 'Medium',3);
INSERT INTO job VALUES (90,9900,'Part-Time',16.00,'Wage', 'Low',4);
INSERT INTO job VALUES (100,2000,'Full-Time',25.00,'Wage', 'Low',5);

INSERT INTO required_by_job VALUES (10,0001);
INSERT INTO required_by_job VALUES (10,0003);
INSERT INTO required_by_job VALUES (20,0001);
INSERT INTO required_by_job VALUES (30,0006);
INSERT INTO required_by_job VALUES (40,0016);
INSERT INTO required_by_job VALUES (50,0010);
INSERT INTO required_by_job VALUES (50,0015);
INSERT INTO required_by_job VALUES (60,0001);
INSERT INTO required_by_job VALUES (70,0024);
INSERT INTO required_by_job VALUES (80,0024);
INSERT INTO required_by_job VALUES (80,0015);
INSERT INTO required_by_job VALUES (90,0008);
INSERT INTO required_by_job VALUES (100,0021);
INSERT INTO required_by_job VALUES (100,0030);

INSERT INTO phone VALUES (1,'803-201-4815', '803-200-3273', '');
INSERT INTO phone VALUES (2,'814-204-1927', '814-201-9499', '');
INSERT INTO phone VALUES (3,'972-240-8926', '972-205-8848', '');
INSERT INTO phone VALUES (4,'201-383-4112', '201-534-2652', '');
INSERT INTO phone VALUES (5,'520-229-6647', '520-230-3559', '');

INSERT INTO works VALUES (1, 10, '30-MAR-16', '04-JUN-18');
INSERT INTO works VALUES (1, 20, '12-JULY-18','16-SEP-20');
INSERT INTO works VALUES (1, 50, '09-OCT-20',NULL);
INSERT INTO works VALUES (2, 20, '21-FEB-18', '12-SEP-19');
INSERT INTO works VALUES (3, 30, '20-MAY-19', NULL);
INSERT INTO works VALUES (4, 40, '12-MAR-12', '29-DEC-18');
INSERT INTO works VALUES (4, 50, '05-JAN-19', NULL);
INSERT INTO works VALUES (5, 50, '09-AUG-17', NULL);

INSERT INTO course VALUES ('LIT824','Technical Writing','intermediate','Teaches principles of writing for business','active','$187.86');
INSERT INTO course VALUES ('CS721','Data Models','Advanced','Teaches skills in information system and database application','expired','$365.58');
INSERT INTO course VALUES ('BS370','Business management','Advanced','Teaches skills needed for effective leadership','expired','$215.98');
INSERT INTO course VALUES ('MCH182','Automotive Engineering','intermediate','Learn skills required to repair machinery', 'expired','$450.67');
INSERT INTO course VALUES ('CS575','C Programming','intermediate','Teaches fundamentals of c programming language and design concepts','active','$643.96');
INSERT INTO course VALUES ('CS496','Java Programming', 'intermediate','Teaches fundamentals of the java programming language and design concepts','active','$647.78');
INSERT INTO course VALUES ('MCH088','CDL Training','Beginner',"Provides training needed to obtain a Commercial Drivers License",'active','$376.23');
INSERT INTO course VALUES ('MATH438','Applied Mathematics','Advanced',"Teaches advanced mathematics and physics",'active','$605.14');
INSERT INTO course VALUES ('LIT477','English I','Beginner','Teaches basic english composition and pronunciation','active','$468.28');
INSERT INTO course VALUES ('CS633','Python','intermediate','Teaches fundamentals of the python programming language and design concepts','active','$464.31');
INSERT INTO course VALUES ('CS654','Machine Learning','intermediate','Learn design principles needed to program artificial intelligence.','active','$532.56'); 

INSERT INTO section VALUES ('LIT824',332,"10-Apr",2018,'UNO','correspondence','$187.86'); 
INSERT INTO section VALUES ('CS721',261,"1-Feb",2019,'Googleplex','classroom','$365.58'); 
INSERT INTO section VALUES ('BS370',335,"04-Dec",2018,'UNO','online self-paced','$215.98'); 
INSERT INTO section VALUES ('MCH182',320,"01-Apr",2018,'General Vehicle','online-sync','$450.67'); 
INSERT INTO section VALUES ('CS575',336,"25-May",2018,'Googleplex','classroom','$643.96'); 
INSERT INTO section VALUES ('CS496',192,"19-Sep",2018,'Googleplex','online self-paced','$647.78'); 
INSERT INTO section VALUES ('MCH088',187,"02-Mar",2019,'Acar Zone','classroom','$376.23'); 
INSERT INTO section VALUES ('MATH438',174,"08-Apr",2020,'UNO','online-sync','$605.14'); 
INSERT INTO section VALUES ('LIT477',380,"02-Jan",2019,'UNO','correspondence','$468.28'); 
INSERT INTO section VALUES ('CS633',190,"24-Dec",2018,'GooglePlex','correspondence','$464.31'); 
INSERT INTO section VALUES ('CS654',203,"16-Jul",2020,'GooglePLex','classroom','$532.56');

INSERT INTO teaches VALUES ('LIT824',0012);
INSERT INTO teaches VALUES ('LIT824',0014);
INSERT INTO teaches VALUES ('LIT824',0008);
INSERT INTO teaches VALUES ('CS721',0003);
INSERT INTO teaches VALUES ('CS721',0015);
INSERT INTO teaches VALUES ('BS370',0007);
INSERT INTO teaches VALUES ('BS370',0008);
INSERT INTO teaches VALUES ('BS370',0017);
INSERT INTO teaches VALUES ('BS370',0018);
INSERT INTO teaches VALUES ('MCH182',0005);
INSERT INTO teaches VALUES ('MCH182',0006);
INSERT INTO teaches VALUES ('CS575',0010);
INSERT INTO teaches VALUES ('CS575',0012);
INSERT INTO teaches VALUES ('CS575',0013);
INSERT INTO teaches VALUES ('CS496',0001);
INSERT INTO teaches VALUES ('CS496',0012);
INSERT INTO teaches VALUES ('CS496',0013);
INSERT INTO teaches VALUES ('MCH008',0013);
INSERT INTO teaches VALUES ('MATH438',0002);
INSERT INTO teaches VALUES ('MATH438',0016);
INSERT INTO teaches VALUES ('MATH438',0013);
INSERT INTO teaches VALUES ('LIT477',0019);
INSERT INTO teaches VALUES ('CS633',0009);
INSERT INTO teaches VALUES ('CS654',0015);
INSERT INTO teaches VALUES ('CS654',0029);

INSERT INTO prerequisite VALUES ('CS721','CS496');
INSERT INTO prerequisite VALUES ('LIT824','LIT447');
INSERT INTO prerequisite VALUES ('CS575','CS496');
INSERT INTO prerequisite VALUES ('CS654','CS721');
INSERT INTO prerequisite VALUES ('CS654','MATH438');

INSERT INTO takes VALUES (1,'CS721',261,'1-Feb-2019');
INSERT INTO takes VALUES (1,'LIT824',332,'10-Apr-2018');
INSERT INTO takes VALUES (2,'BS370',335,'04-Dec-2020');
INSERT INTO takes VALUES (2,'CS575',336,'25-May-2018');
INSERT INTO takes VALUES (4,'BS370',335,'04-Dec-2020');
INSERT INTO takes VALUES (4,'MCH182',320,'01-Apr-2018');
INSERT INTO takes VALUES (5,'CS654',203,'16-Jul-2020');





